// BrucesFirstProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h" // only comes up when you don't make an empty project
#include <iostream>
#include <string>
using namespace std;

int main()
{
	string name; // string for letters or words
	string gender;
	int age; // int for numbers
	// declare every variable (everything you're going to use) first
	
	cout << "Enter your name:";
	cin >> name;
	cout << "Enter your age:";
	cin >> age;
	cout << "Enter your gender (M/F):";
	cin >> gender;
	system ("cls"); // clears the screen

	if (gender == "M" || gender == "m") // put quotation marks on the variables 
	{
		cout << "Hello " << name << ", young lad" << ", aged " << age;
		cout << endl << endl;
	}
	else if (gender == "F" || gender == "f")
	{
		cout << "Hello " << name << ", m'lady" << ", aged " << age;
		cout << endl << endl;
	}
	else
	{
		cout << "What did you do? Can't you follow simple instructions? Huh?";
		cout << endl << endl;
	}
	system("pause"); //pauses or stops the program (Press any key to continue...)

    return 0;
}

